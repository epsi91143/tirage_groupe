from student import Student
import random

def create_list_student() -> list :
    '''
    Fonction pour créer la liste des étudiants en objet 
    Cette fonction trie de manière aléatoires les élèves dans la list
    '''
    file1 = open('promotion_B3_B.csv')
    list_student = []
    count = -1
    for line in file1:
        count+=1
        if count == 0 : 
            continue
        student = line.removesuffix('\n').split(';')
        list_student.append(Student(student[0],student[1]))
    file1.close()
    random.shuffle(list_student)
    return list_student

def create_groupe(list_student) -> list : 
    '''
    Créer les différents groupes en fonction d'une liste d'étudiant
    '''
    list_group = []
    num_groupe = 0
    while len(list_student) > 0 :
        num_groupe+=1
        if len(list_student) == 1 : 
            list_group.append({f"{num_groupe}" : [list_student[0]]})
            list_student.remove(list_student[0])
        else :
            list_group.append({f"{num_groupe}" : [list_student[0], list_student[-1]]})
            list_student.remove(list_student[0])
            list_student.remove(list_student[-1])
    return list_group

def create_file_group(list_groupe) : 
    '''
    Créer le fichier de sortie
    '''
    f = open("groupe.csv", "w")
    for groupe in list_groupe:
        for num,eleves in groupe.items() :
            if len(eleves) == 1 : 
                f.write(f"Groupe {num};{eleves[0].nom};{eleves[0].prenom}\n")
            else : 
                f.write(f"Groupe {num};{eleves[0].nom};{eleves[0].prenom};{eleves[1].nom};{eleves[1].prenom}\n")

