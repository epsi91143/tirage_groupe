"""
 ALA : 06/12/2023
 Script permettant de créer des groupes en fonction du fichier promotion_B3_B.csv
"""
import function

if __name__ == "__main__":
    print("Lancement du programme de tirage !")
    list_student = function.create_list_student()
    list_groupe = function.create_groupe(list_student)
    function.create_file_group(list_groupe)
    print("Fin du programme de tirage !")

